Instruction for installation:
****************************
You can copy the snippet file to : 
    C:\Users\ {USERNAME} \AppData\Roaming\Code\User\snippets

If you want to create a new snippet file (to update it regarding EDN methode and property):
    place yourself inside the folder where EdnParser.js is.
    And type : (require node.JS installed in your computer)
        1. npm install (Install depedency)
        2. node EdnParser.js (Generate snippet file)


Description:
****************************
A node.JS script that scrape EDN of Efficy and generate a snippet file that can be used with VSCode.


****************************
Release 1.0
-----------
    -> Web scrape every information on EDN and generate a snippet file. Care because property are hardcoded in this version.
